version: '2'
services:
  ping:
    image: hashicorp/http-echo
    stdin_open: true
    tty: true
    command:
    - -text
    - Pong
    labels:
      io.rancher.container.pull_image: always
  minio:
    image: minio/minio:RELEASE.2017-09-29T19-16-56Z
    environment:
      MINIO_ACCESS_KEY: '${S3_ACCESS_KEY}'
      MINIO_SECRET_KEY: '${S3_SECRET_KEY}'
    stdin_open: true
    tty: true
    command:
    - server
    - /minio
    volumes:
    - minio:/minio
    volume_driver: '${VOLUME_DRIVER}'
    labels:
      io.rancher.container.pull_image: always
      {{- if eq .Values.VOLUME_DRIVER "rexray-dobs" }}
      br.com.ossystems.rancher.backup.driver: dobs
      br.com.ossystems.rancher.backup.schedule: 0 0 0 * * ?
      br.com.ossystems.rancher.backup.dobs.volume: minio
      br.com.ossystems.rancher.backup.dobs.digitalocean_access_token:env: rexray-dobs-driver:DOBS_TOKEN
      {{- end }}
  monitor:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/api-server:v17.1.0-196-g8bb11b9
    environment:
      RAILS_ENV: production
      DATABASE_HOST: postgres
      DATABASE_NAME: updatehub
      DATABASE_USER: postgres
      DATABASE_PASSWORD: postgres
    stdin_open: true
    entrypoint:
    - /usr/local/bundle/bin/rake
    tty: true
    links:
    - postgres:postgres
    - redis:redis
    command:
    - rollout:monitor
    labels:
      io.rancher.container.pull_image: always
  redis:
    image: redis:4.0.2
    stdin_open: true
    tty: true
    labels:
      io.rancher.container.pull_image: always
  hydra-automigrate:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/hydra-bootstrap:e6db48f808d2476c6ad84d8d3c192250da2030cb
    environment:
      DATABASE_HOST: mariadb
      DATABASE_NAME: hydra
      DATABASE_USER: root
      DATABASE_PASSWORD: root
      FORCE_ROOT_CLIENT_CREDENTIALS: '${HYDRA_ROOT_CLIENT}:${HYDRA_ROOT_SECRET}'
    stdin_open: true
    tty: true
    links:
    - mariadb:mariadb
    command:
    - hydra-automigrate
    labels:
      io.rancher.container.pull_image: always
      io.rancher.container.start_once: 'true'
  mariadb:
    image: mariadb:10.2.9
    environment:
      MYSQL_ROOT_PASSWORD: root
    volume_driver: '${VOLUME_DRIVER}'
    volumes:
      - mariadb:/var/lib/mysql
    stdin_open: true
    tty: true
    command:
      - --ignore-db-dir
      - lost+found
    labels:
      io.rancher.container.pull_image: always
      br.com.ossystems.rancher.backup.driver: mysql
      br.com.ossystems.rancher.backup.schedule: 0 0 0 * * ?
  hydra:
    image: oryd/hydra:v0.9.13-http
    environment:
      LOG_LEVEL: info
      SYSTEM_SECRET: '${HYDRA_SYSTEM_SECRET}'
      CONSENT_URL: 'http://auth.${UPDATEHUB_DOMAIN}/auth/consent'
      DATABASE_URL: 'mysql://root:root@tcp(mariadb:3306)/hydra?parseTime=true'
      FORCE_ROOT_CLIENT_CREDENTIALS: '${HYDRA_ROOT_CLIENT}:${HYDRA_ROOT_SECRET}'
      ISSUER: 'http://login.${UPDATEHUB_DOMAIN}'
    stdin_open: true
    tty: true
    links:
    - mariadb:mariadb
    volumes_from:
    - hydra-automigrate
    labels:
      io.rancher.container.pull_image: always
      io.rancher.sidekicks: hydra-automigrate
  api-hydra-create-policies:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/api-server:v17.1.0-196-g8bb11b9
    environment:
      RAILS_ENV: production
      HYDRA_CLIENT_ID: '${HYDRA_ROOT_CLIENT}'
      HYDRA_CLIENT_SECRET: '${HYDRA_ROOT_SECRET}'
      HYDRA_HOST: http://hydra:4444/
    entrypoint:
    - /usr/local/bundle/bin/rake
    stdin_open: true
    tty: true
    links:
    - postgres:postgres
    - hydra:hydra
    command:
    - hydra:create_policies
    labels:
      io.rancher.container.pull_image: always
      io.rancher.container.start_once: 'true'
  idp:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/auth-server:63d39e8
    environment:
      DATABASE_HOST: mariadb
      DATABASE_PORT: '3306'
      DATABASE_NAME: idp
      DATABASE_USER: root
      DATABASE_PASSWORD: root
      HYDRA_HOST: http://hydra:4444
      HYDRA_CLIENT_ID: '${IDP_HYDRA_CLIENT}'
      HYDRA_CLIENT_SECRET: '${IDP_HYDRA_SECRET}'
      SECRET_KEY: '${IDP_SECRET_KEY}'
      SMTP_SERVER_ADDRESS: smtp.gmail.com
      SMTP_PORT: '587'
      SMTP_FROM_EMAIL: updatehub@ossystems.com.br
      SMTP_USERNAME: smtp@ossystems.com.br
      SMTP_PASSWORD: gsblkuypjmmjomzf
      SMTP_USE_TLS: '1'
    stdin_open: true
    tty: true
    links:
    - hydra:hydra
    - mariadb:mariadb
    volumes_from:
    - idp-hydra-import
    labels:
      io.rancher.container.pull_image: always
      io.rancher.sidekicks: idp-hydra-import
  ui:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/ui:467ae8a
    environment:
      API_URL: 'http://api.${UPDATEHUB_DOMAIN}'
      APP_URL: 'http://dashboard.${UPDATEHUB_DOMAIN}'
      OAUTH2_URL: 'http://login.${UPDATEHUB_DOMAIN}/oauth2/auth'
      OAUTH2_ID: '${UI_HYDRA_CLIENT}'
      OAUTH2_REDIR_URL: 'http://dashboard.${UPDATEHUB_DOMAIN}/auth/callback'
      HOME_PAGE_URL: 'http://www.${UPDATEHUB_DOMAIN}'
    stdin_open: true
    tty: true
    links:
    - hydra:hydra
    volumes_from:
    - ui-hydra-bootstrap
    labels:
      io.rancher.container.pull_image: always
      io.rancher.sidekicks: ui-hydra-bootstrap
  lb:
    image: rancher/lb-service-haproxy:v0.7.5
    ports:
    - 80:80/tcp
    labels:
      io.rancher.container.agent.role: environmentAdmin
      io.rancher.container.create_agent: 'true'
      io.rancher.scheduler.global: 'true'
  api:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/api-server:v17.1.0-196-g8bb11b9
    environment:
      RAILS_ENV: production
      DATABASE_HOST: postgres
      DATABASE_NAME: updatehub
      DATABASE_USER: postgres
      DATABASE_PASSWORD: postgres
      DEVISE_SECRET_KEY: ''
      HYDRA_CLIENT_ID: '${API_HYDRA_CLIENT}'
      HYDRA_CLIENT_SECRET: '${API_HYDRA_SECRET}'
      HYDRA_HOST: http://hydra:4444/
      SECRET_KEY_BASE: '${API_SECRET_KEY}'
      S3_ACCESS_KEY: '${S3_ACCESS_KEY}'
      S3_SECRET_KEY: '${S3_SECRET_KEY}'
      S3_BUCKET: '${S3_BUCKET}'
      S3_ENDPOINT: 'http://storage.${UPDATEHUB_DOMAIN}'
    stdin_open: true
    entrypoint:
    - /usr/local/bin/start-server.sh
    tty: true
    links:
    - postgres:postgres
    - hydra:hydra
    volumes_from:
    - api-hydra-create-policies
    - api-bootstrap
    labels:
      io.rancher.container.pull_image: always
      io.rancher.sidekicks: api-hydra-create-policies,api-bootstrap,api-hydra-import
  idp-hydra-import:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/hydra-bootstrap:e6db48f808d2476c6ad84d8d3c192250da2030cb
    environment:
      HOST: http://hydra:4444
      CLIENT_ID: '${HYDRA_ROOT_CLIENT}'
      CLIENT_SECRET: '${HYDRA_ROOT_SECRET}'
      HYDRA_CLIENT_ID: '${IDP_HYDRA_CLIENT}'
      HYDRA_CLIENT_SECRET: '${IDP_HYDRA_SECRET}'
      HYDRA_CLIENT_GRANT_TYPE: client_credentials
      HYDRA_CLIENT_SCOPE: hydra.keys.get hydra.clients
      HYDRA_POLICY_RESOURCES: rn:hydra:keys:hydra.consent.response:private,rn:hydra:clients:<.*>,rn:hydra:keys:hydra.consent.challenge:private,rn:hydra:keys:hydra.consent.challenge:public
      HYDRA_POLICY_ACTIONS: get
      HYDRA_POLICY_ID: 7f1f44be-cc87-4b5a-97ed-faf0eb51e92d
      HYDRA_CLIENT_RESPONSE_TYPE: token
    stdin_open: true
    tty: true
    links:
    - hydra:hydra
    - mariadb:mariadb
    command:
    - hydra-import
    labels:
      io.rancher.container.pull_image: always
      io.rancher.container.start_once: 'true'
  worker:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/api-server:v17.1.0-196-g8bb11b9
    environment:
      RAILS_ENV: production
      DATABASE_HOST: postgres
      DATABASE_NAME: updatehub
      DATABASE_USER: postgres
      DATABASE_PASSWORD: postgres
      QUEUE: '*'
      DEVISE_SECRET_KEY: '1'
      S3_ACCESS_KEY: '${S3_ACCESS_KEY}'
      S3_SECRET_KEY: '${S3_SECRET_KEY}'
      S3_BUCKET: '${S3_BUCKET}'
      S3_ENDPOINT: 'http://storage.${UPDATEHUB_DOMAIN}'
      SMTP_SERVER_ADDRESS: smtp.gmail.com
      SMTP_PORT: '587'
      SMTP_DOMAIN: ossystems.com.br
      SMTP_USERNAME: smtp@ossystems.com.br
      SMTP_PASSWORD: gsblkuypjmmjomzf
      SMTP_USE_TLS: '1'
      SMTP_FROM_EMAIL: updatehub@ossystems.com.br
    stdin_open: true
    entrypoint:
    - /usr/local/bundle/bin/rake
    tty: true
    links:
    - postgres:postgres
    - minio:minio
    - redis:redis
    command:
    - resque:work
    labels:
      io.rancher.container.pull_image: always
  api-bootstrap:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/api-server:v17.1.0-196-g8bb11b9
    environment:
      RAILS_ENV: production
      DATABASE_HOST: postgres
      DATABASE_NAME: updatehub
      DATABASE_USER: postgres
      DATABASE_PASSWORD: postgres
      PGUSER: postgres
      S3_ACCESS_KEY: '${S3_ACCESS_KEY}'
      S3_SECRET_KEY: '${S3_SECRET_KEY}'
      S3_BUCKET: '${S3_BUCKET}'
      S3_ENDPOINT: 'http://storage.${UPDATEHUB_DOMAIN}'
    entrypoint:
    - /usr/local/bundle/bin/rake
    stdin_open: true
    tty: true
    links:
    - postgres:postgres
    - hydra:hydra
    command:
    - db:migrate
    - s3:create_bucket
    labels:
      io.rancher.container.pull_image: always
      io.rancher.container.start_once: 'true'
  postgres:
    image: postgres:9.6.3-alpine
    environment:
      POSTGRES_USER: postgres
      POSTGRES_PASSWORD: postgres
      PGDATA: /var/lib/postgresql/data/pgdata
    volume_driver: '${VOLUME_DRIVER}'
    volumes:
      - postgres:/var/lib/postgresql/data
    stdin_open: true
    tty: true
    labels:
      io.rancher.container.pull_image: always
      br.com.ossystems.rancher.backup.driver: postgres
      br.com.ossystems.rancher.backup.schedule: 0 0 0 * * ?
  ui-hydra-bootstrap:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/hydra-bootstrap:e6db48f808d2476c6ad84d8d3c192250da2030cb
    environment:
      HOST: http://hydra:4444
      CLIENT_ID: '${HYDRA_ROOT_CLIENT}'
      CLIENT_SECRET: '${HYDRA_ROOT_SECRET}'
      HYDRA_CLIENT_ID: '${UI_HYDRA_CLIENT}'
      HYDRA_CLIENT_SECRET: ''
      HYDRA_CLIENT_GRANT_TYPE: implicit
      HYDRA_CLIENT_RESPONSE_TYPE: token
      HYDRA_CLIENT_REDIRECT_URI: 'http://dashboard.${UPDATEHUB_DOMAIN}/auth/callback'
    stdin_open: true
    tty: true
    links:
    - hydra:hydra
    command:
    - hydra-import
    labels:
      io.rancher.container.pull_image: always
      io.rancher.container.start_once: 'true'
  api-hydra-import:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/hydra-bootstrap:e6db48f808d2476c6ad84d8d3c192250da2030cb
    environment:
      HOST: http://hydra:4444
      CLIENT_ID: '${HYDRA_ROOT_CLIENT}'
      CLIENT_SECRET: '${HYDRA_ROOT_SECRET}'
      HYDRA_CLIENT_ID: '${API_HYDRA_CLIENT}'
      HYDRA_CLIENT_SECRET: '${API_HYDRA_SECRET}'
      HYDRA_CLIENT_GRANT_TYPE: client_credentials
      HYDRA_CLIENT_SCOPE: hydra.policies hydra.warden
      HYDRA_POLICY_RESOURCES: rn:hydra:policies,rn:hydra:policies:<.*>,rn:hydra:warden:allowed
      HYDRA_POLICY_ACTIONS: decide,create,update,get,list
      HYDRA_POLICY_ID: '${API_HYDRA_POLICY}'
      HYDRA_CLIENT_RESPONSE_TYPE: code
    stdin_open: true
    tty: true
    links:
    - postgres:postgres
    - hydra:hydra
    command:
    - hydra-import
    labels:
      io.rancher.container.pull_image: always
      io.rancher.container.start_once: 'true'

