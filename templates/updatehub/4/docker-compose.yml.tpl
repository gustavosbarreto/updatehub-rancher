version: '2'
services:
  api-proxy:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/api-proxy:18.9.0
    environment:
      ENV: 'production'
      AMQP_URL: 'amqp://guest:guest@rabbitmq:5672'
    links:
    - api:api
    stdin_open: true
    labels:
      io.rancher.container.pull_image: always
  mender-api-proxy:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/mender-api-proxy:18.9.0
    environment:
      JWT_SECRET: 4fe86475098425c29ca11717eaa09efab96477bc
      MONGO_HOST: mongo
      UPDATEHUB_SERVER_URL: http://api-proxy:8080
    stdin_open: true
    links:
    - mongo:mongo
    labels:
      io.rancher.container.pull_image: always
  namespace-auth:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/namespace-auth:18.12.0
    environment:
      DATABASE_HOST: postgres
      DATABASE_NAME: updatehub
      DATABASE_USER: postgres
      DATABASE_PASSWORD: postgres
      HYDRA_PUBLIC_HOST: 'http://hydra:4444'
      HYDRA_ADMIN_HOST: 'http://hydra:4445'
      HYDRA_CLIENT_ID: '${NAMESPACEAUTH_HYDRA_CLIENT}'
      HYDRA_CLIENT_SECRET: '${NAMESPACEAUTH_HYDRA_SECRET}'
    stdin_open: true
    tty: true
    links:
    - postgres:postgres
    - hydra:hydra
    labels:
      io.rancher.container.pull_image: always
      io.rancher.sidekicks: namespace-auth-hydra-bootstrap
  namespace-auth-hydra-bootstrap:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/hydra-bootstrap:19.7.0
    environment:
      CLIENT_ID: '${HYDRA_ROOT_CLIENT}'
      CLIENT_SECRET: '${HYDRA_ROOT_SECRET}'
      HYDRA_ADMIN_URL: 'http://hydra:4445'
      HYDRA_CLIENT_ID: '${NAMESPACEAUTH_HYDRA_CLIENT}'
      HYDRA_CLIENT_SECRET: '${NAMESPACEAUTH_HYDRA_SECRET}'
      HYDRA_CLIENT_SCOPE: hydra.policies hydra.warden
      HYDRA_CLIENT_GRANT_TYPE: client_credentials
      HYDRA_CLIENT_RESPONSE_TYPE: token
    stdin_open: true
    tty: true
    links:
    - hydra:hydra
    command:
    - hydra-import
    labels:
      io.rancher.container.pull_image: always
      io.rancher.container.start_once: 'true'
  monitor:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/api-server:19.2.0
    environment:
      RAILS_ENV: production
      DATABASE_HOST: postgres
      DATABASE_NAME: updatehub
      DATABASE_USER: postgres
      DATABASE_PASSWORD: postgres
    stdin_open: true
    entrypoint:
    - /usr/local/bin/bundle
    tty: true
    links:
    - postgres:postgres
    - redis:redis
    command:
    - exec
    - rake
    - rollout:monitor
    labels:
      io.rancher.container.pull_image: always
  goldy:
    image: ossystems/goldy:1.0.0
    environment:
      KEY: '${GOLDY_KEY}'
      CERT: '${GOLDY_CERT}'
      KEY_FILENAME: /etc/goldy.key
      CERT_FILENAME: /etc/goldy.cert
    ports:
    - 5684:5684/udp
    links:
    - cdn:cdn
    command:
    - -l
    - 0.0.0.0:5684
    - -b
    - cdn:5683
    - -c
    - /etc/goldy.cert
    - -k
    - /etc/goldy.key
    stdin_open: true
    tty: true
    labels:
      io.rancher.container.pull_image: always
  cdn:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/cdn-monitor:19.3.3
    environment:
      AMQP_URL: amqp://guest:guest@rabbitmq:5672
    command:
    - --coap
    - 0.0.0.0:5683
    - --monitor
    - /monitor/cdn-monitor.so
    - --backend
    - http://api:9292
    - --cache
    - products/.*/packages/.*/objects/.*
    ports:
    - 5683:5683/udp
    stdin_open: true
    tty: true
    labels:
      io.rancher.container.pull_image: always
  billing-api:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/billing-service:19.12.0
    environment:
      ENV: production
      AMQP_URL: 'amqp://guest:guest@rabbitmq:5672'
      PGHOST: postgres
      PGUSER: postgres
      PGPASSWORD: postgres
      PGDATABASE: billing-service
      PGSSLMODE: disable
      INFLUX_HOST: http://influxdb:8086
      AUTH_SERVER: http://namespace-auth:8081
      MONGO_HOST: mongo
      SLACK_TOKEN: '${SLACK_TOKEN}'
      SLACK_USER: '${SLACK_USER}'
      SLACK_CHANNEL: '${SLACK_CHANNEL}'
      FS_API_USERNAME: '${FS_API_USERNAME}'
      FS_API_PASSWORD: '${FS_API_PASSWORD}'
      FS_WEBHOOK_HMAC: '${FS_WEBHOOK_HMAC}'
      SENDER: '${BILLING_SENDER}'
      TEMPLATE_PAYMENTCONFIRMID: '${BILLING_TEMPLATE_PAYMENTCONFIRMID}'
      TEMPLATE_NAMESPACEBLOCKEDID: '${BILLING_TEMPLATE_NAMESPACEBLOCKEDID}'
      SENDGRID_API_KEY: '${BILLING_SENDGRID_API_KEY}'
    stdin_open: true
    command:
    - /usr/local/updatehub/api-server
    tty: true
    links:
    - rabbitmq:rabbitmq
    - postgres:postgres
    - mongo:mongo
    - influxdb:influxdb
    - namespace-auth:namespace-auth
    labels:
      io.rancher.container.pull_image: always
  billing-bot:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/billing-service:19.12.0
    environment:
      ENV: production
      AMQP_URL: 'amqp://guest:guest@rabbitmq:5672'
      PGHOST: postgres
      PGUSER: postgres
      PGPASSWORD: postgres
      PGDATABASE: billing-service
      PGSSLMODE: disable
      INFLUX_HOST: http://influxdb:8086
      AUTH_SERVER: http://namespace-auth:8081
      MONGO_HOST: mongo
      SLACK_TOKEN: '${SLACK_TOKEN}'
      SLACK_USER: '${SLACK_USER}'
      SLACK_CHANNEL: '${SLACK_CHANNEL}'
      FS_API_USERNAME: '${FS_API_USERNAME}'
      FS_API_PASSWORD: '${FS_API_PASSWORD}'
      FS_WEBHOOK_HMAC: '${FS_WEBHOOK_HMAC}'
      SENDER: '${BILLING_SENDER}'
      TEMPLATE_PAYMENTCONFIRMID: '${BILLING_TEMPLATE_PAYMENTCONFIRMID}'
      TEMPLATE_NAMESPACEBLOCKEDID: '${BILLING_TEMPLATE_NAMESPACEBLOCKEDID}'
      SENDGRID_API_KEY: '${BILLING_SENDGRID_API_KEY}'
    stdin_open: true
    command:
    - /usr/local/updatehub/bot
    tty: true
    links:
    - rabbitmq:rabbitmq
    - postgres:postgres
    - mongo:mongo
    - influxdb:influxdb
    - namespace-auth:namespace-auth
    labels:
      io.rancher.container.pull_image: always
  auditmq:
    image: ossystems/auditmq:19.1.0
    stdin_open: true
    tty: true
    labels:
      io.rancher.container.pull_image: always
    environment:
      CONFIG: '${AUDITMQ_CONFIG}'
    links:
    - rabbitmq:rabbitmq
  rabbitmq:
    image: rabbitmq:3.7.12-management-alpine
    stdin_open: true
    tty: true
    volume_driver: '${VOLUME_DRIVER}'
    volumes:
      - rabbitmq:/var/lib/rabbitmq
    labels:
      io.rancher.container.pull_image: always
    environment:
      RABBITMQ_ERLANG_COOKIE: '${RABBITMQ_COOKIE}'
      RABBITMQ_NODENAME: 'rabbitmq@rabbitmq'
  redis:
    image: redis:4.0.9-alpine
    stdin_open: true
    tty: true
    labels:
      io.rancher.container.pull_image: always
  hydra-automigrate:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/hydra-bootstrap:19.7.0
    environment:
      DATABASE_HOST: mariadb
      DATABASE_NAME: hydra
      DATABASE_USER: root
      DATABASE_PASSWORD: root
    stdin_open: true
    tty: true
    links:
    - mariadb:mariadb
    command:
    - hydra-automigrate
    labels:
      io.rancher.container.pull_image: always
      io.rancher.container.start_once: 'true'
  mariadb:
    image: mariadb:10.3.3
    environment:
      MYSQL_ROOT_PASSWORD: root
    volume_driver: '${VOLUME_DRIVER}'
    volumes:
      - mariadb:/var/lib/mysql
    stdin_open: true
    tty: true
    command:
      - --ignore-db-dir
      - lost+found
    labels:
      io.rancher.container.pull_image: always
      br.com.ossystems.rancher.backup.driver: mysql
      br.com.ossystems.rancher.backup.schedule: 0 0 0 * * ?
  mongo:
    image: mongo:3.6.3
    volume_driver: '${VOLUME_DRIVER}'
    volumes:
      - mongo:/data/db
    labels:
      io.rancher.container.pull_image: always
  hydra:
    image: oryd/hydra:v1.0.0
    environment:
      URLS_SELF_ISSUER: 'https://login.${UPDATEHUB_DOMAIN}'
      URLS_CONSENT: 'https://auth.${UPDATEHUB_DOMAIN}/auth/consent'
      URLS_LOGIN: 'https://auth.${UPDATEHUB_DOMAIN}/auth/login'
      DSN: mysql://root:root@tcp(mariadb:3306)/hydra?parseTime=true
      SECRETS_SYSTEM: '${HYDRA_SYSTEM_SECRET}'
    stdin_open: true
    tty: true
    command:
      - serve
      - all
      - --dangerous-force-http
    links:
    - mariadb:mariadb
    volumes_from:
    - hydra-automigrate
    labels:
      io.rancher.container.pull_image: always
      io.rancher.sidekicks: hydra-automigrate
  api-hydra-create-policies:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/api-server:19.2.0
    environment:
      RAILS_ENV: production
      KETO_URL: 'http://keto:4466/'
    entrypoint:
    - /usr/local/bin/bundle
    stdin_open: true
    tty: true
    links:
    - postgres:postgres
    - hydra:hydra
    - keto:keto
    command:
    - exec
    - rake
    - keto:create_policies
    labels:
      io.rancher.container.pull_image: always
      io.rancher.container.start_once: 'true'
  keto:
    image: oryd/keto:v1.0.0-beta.9
    environment:
      DATABASE_URL: mysql://root:root@tcp(mariadb:3306)/keto?parseTime=true
    stdin_open: true
    tty: true
    links:
    - mariadb:mariadb
    labels:
      io.rancher.sidekicks: keto-automigrate
      io.rancher.container.pull_image: always
  keto-automigrate:
    image: oryd/keto:v1.0.0-beta.9
    environment:
      DATABASE_URL: mysql://root:root@tcp(mariadb:3306)/keto?parseTime=true
    stdin_open: true
    tty: true
    links:
    - mariadb:mariadb
    command:
      - migrate
      - sql
      - --read-from-env
    labels:
      io.rancher.container.pull_image: always
      io.rancher.container.start_once: 'true'
  idp:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/auth-server:19.7.0
    environment:
      DATABASE_HOST: mariadb
      DATABASE_PORT: '3306'
      DATABASE_NAME: idp
      DATABASE_USER: root
      DATABASE_PASSWORD: root
      HYDRA_ADMIN_HOST: http://hydra:4445
      HYDRA_PUBLIC_HOST: http://hydra:4444
      HYDRA_CLIENT_ID: '${IDP_HYDRA_CLIENT}'
      HYDRA_CLIENT_SECRET: '${IDP_HYDRA_SECRET}'
      SECRET_KEY: '${IDP_SECRET_KEY}'
      SMTP_SERVER_ADDRESS: smtp.gmail.com
      SMTP_PORT: '587'
      SMTP_FROM_EMAIL: '"updatehub - by O.S. Systems" <no-reply@ossystems.com.br>'
      SMTP_USERNAME: no-reply@ossystems.com.br
      SMTP_PASSWORD: gsblkuypjmmjomzf
      SMTP_USE_TLS: '1'
      APP_URL: 'https://dashboard.${UPDATEHUB_DOMAIN}'
    stdin_open: true
    tty: true
    links:
    - hydra:hydra
    - mariadb:mariadb
    volumes_from:
    - idp-hydra-import
    labels:
      io.rancher.container.pull_image: always
      io.rancher.sidekicks: idp-hydra-import,idp-create-user
  idp-create-user:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/auth-server:19.7.0
    environment:
      DATABASE_HOST: mariadb
      DATABASE_PORT: '3306'
      DATABASE_NAME: idp
      DATABASE_USER: root
      DATABASE_PASSWORD: root
      HYDRA_ADMIN_HOST: http://hydra:4445
      HYDRA_PUBLIC_HOST: http://hydra:4444
      HYDRA_CLIENT_ID: '${IDP_HYDRA_CLIENT}'
      HYDRA_CLIENT_SECRET: '${IDP_HYDRA_SECRET}'
      SECRET_KEY: '${IDP_SECRET_KEY}'
      SMTP_SERVER_ADDRESS: smtp.gmail.com
      SMTP_PORT: '587'
      SMTP_FROM_EMAIL: '"updatehub - by O.S. Systems" <no-reply@ossystems.com.br>'
      SMTP_USERNAME: no-reply@ossystems.com.br
      SMTP_PASSWORD: gsblkuypjmmjomzf
      SMTP_USE_TLS: '1'
      IDP_ALLOW_NEW_USERS: 'no'
      IDP_ROOT_USERNAME: admin
      IDP_ROOT_EMAIL: admin@updatehub.io
      IDP_ROOT_PASSWORD: 0cLa0qUVytPI
    stdin_open: true
    tty: true
    links:
    - hydra:hydra
    - mariadb:mariadb
    command:
    - /usr/local/app/manage.py createrootuser
    labels:
      io.rancher.container.pull_image: always
      io.rancher.container.start_once: 'true'
  ui:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/ui:19.12.0
    environment:
      API_URL: 'https://api.${UPDATEHUB_DOMAIN}'
      APP_URL: 'https://dashboard.${UPDATEHUB_DOMAIN}'
      OAUTH2_URL: 'https://login.${UPDATEHUB_DOMAIN}/oauth2/auth'
      OAUTH2_ID: '${UI_HYDRA_CLIENT}'
      OAUTH2_REDIR_URL: 'https://dashboard.${UPDATEHUB_DOMAIN}/auth/callback'
      IDP_LOGOUT_URL: 'https://auth.${UPDATEHUB_DOMAIN}/auth/logout'
      EDIT_PROFILE_URL: 'https://auth.${UPDATEHUB_DOMAIN}'
      BILLING_API_URL: 'https://billing.${UPDATEHUB_DOMAIN}'
      FS_ACCESS_KEY: '${FS_ACCESS_KEY}'
      FS_STORE_FRONT: '${FS_STORE_FRONT}'
    stdin_open: true
    tty: true
    links:
    - hydra:hydra
    volumes_from:
    - ui-hydra-bootstrap
    labels:
      io.rancher.container.pull_image: always
      io.rancher.sidekicks: ui-hydra-bootstrap
  lb:
    image: rancher/lb-service-haproxy:v0.9.6
    ports:
    - 443:443/tcp
    - 80:80/tcp
    labels:
      io.rancher.container.agent.role: environmentAdmin
      io.rancher.container.create_agent: 'true'
      io.rancher.scheduler.global: 'true'
  api:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/api-server:19.2.0
    environment:
      RAILS_ENV: production
      DATABASE_HOST: postgres
      DATABASE_NAME: updatehub
      DATABASE_USER: postgres
      DATABASE_PASSWORD: postgres
      DEVISE_SECRET_KEY: ''
      HYDRA_ADMIN_URL: 'http://hydra:4445/'
      SECRET_KEY_BASE: '${API_SECRET_KEY}'
      KETO_URL: 'http://keto:4466/'
      S3_ACCESS_KEY: '${S3_ACCESS_KEY}'
      S3_SECRET_KEY: '${S3_SECRET_KEY}'
      S3_BUCKET: '${S3_BUCKET}'
      S3_REGION: '${S3_REGION}'
      S3_ENDPOINT: '${S3_ENDPOINT}'
    stdin_open: true
    entrypoint:
    - /usr/local/bin/start-server.sh
    links:
    - rabbitmq:rabbitmq
    - postgres:postgres
    - hydra:hydra
    - keto:keto
    volumes_from:
    - api-hydra-create-policies
    - api-bootstrap
    labels:
      io.rancher.container.pull_image: always
      io.rancher.sidekicks: api-hydra-create-policies,api-bootstrap
  api-reporter:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/api-server:19.2.0
    environment:
      RAILS_ENV: production
      DATABASE_HOST: postgres
      DATABASE_NAME: updatehub
      DATABASE_USER: postgres
      DATABASE_PASSWORD: postgres
    stdin_open: true
    entrypoint:
    - /usr/local/bin/bundle
    tty: true
    links:
    - rabbitmq:rabbitmq
    - postgres:postgres
    - redis:redis
    command:
    - exec
    - rake
    - metrics:reporter
    labels:
      io.rancher.container.pull_image: always
  idp-hydra-import:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/hydra-bootstrap:19.7.0
    environment:
      CLIENT_ID: '${HYDRA_ROOT_CLIENT}'
      CLIENT_SECRET: '${HYDRA_ROOT_SECRET}'
      HYDRA_ADMIN_URL: 'http://hydra:4445'
      HYDRA_CLIENT_ID: '${IDP_HYDRA_CLIENT}'
      HYDRA_CLIENT_SECRET: '${IDP_HYDRA_SECRET}'
      HYDRA_CLIENT_GRANT_TYPE: client_credentials
      HYDRA_CLIENT_SCOPE: hydra.keys.get hydra.clients
      HYDRA_CLIENT_RESPONSE_TYPE: token
    stdin_open: true
    tty: true
    links:
    - hydra:hydra
    - mariadb:mariadb
    command:
    - hydra-import
    labels:
      io.rancher.container.pull_image: always
      io.rancher.container.start_once: 'true'
  worker:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/api-server:19.2.0
    environment:
      RAILS_ENV: production
      DATABASE_HOST: postgres
      DATABASE_NAME: updatehub
      DATABASE_USER: postgres
      DATABASE_PASSWORD: postgres
      QUEUE: '*'
      DEVISE_SECRET_KEY: '1'
      S3_ACCESS_KEY: '${S3_ACCESS_KEY}'
      S3_SECRET_KEY: '${S3_SECRET_KEY}'
      S3_BUCKET: '${S3_BUCKET}'
      S3_REGION: '${S3_REGION}'
      S3_ENDPOINT: '${S3_ENDPOINT}'
      SMTP_SERVER_ADDRESS: smtp.gmail.com
      SMTP_PORT: '587'
      SMTP_DOMAIN: ossystems.com.br
      SMTP_USERNAME: no-reply@ossystems.com.br
      SMTP_PASSWORD: gsblkuypjmmjomzf
      SMTP_USE_TLS: '1'
      SMTP_FROM_EMAIL: '"updatehub - by O.S. Systems" <no-reply@ossystems.com.br>'
    stdin_open: true
    entrypoint:
    - /usr/local/bin/bundle
    tty: true
    links:
    - postgres:postgres
    - redis:redis
    - rabbitmq:rabbitmq
    command:
    - exec
    - rake
    - resque:work
    labels:
      io.rancher.container.pull_image: always
  api-limit-consumer:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/api-server:19.2.0
    environment:
      RAILS_ENV: production
      DATABASE_HOST: postgres
      DATABASE_NAME: updatehub
      DATABASE_USER: postgres
      DATABASE_PASSWORD: postgres
      QUEUE: '*'
      DEVISE_SECRET_KEY: '1'
      S3_ACCESS_KEY: '${S3_ACCESS_KEY}'
      S3_SECRET_KEY: '${S3_SECRET_KEY}'
      S3_BUCKET: '${S3_BUCKET}'
      S3_REGION: '${S3_REGION}'
      S3_ENDPOINT: '${S3_ENDPOINT}'
      SMTP_SERVER_ADDRESS: smtp.gmail.com
      SMTP_PORT: '587'
      SMTP_DOMAIN: ossystems.com.br
      SMTP_USERNAME: no-reply@ossystems.com.br
      SMTP_PASSWORD: gsblkuypjmmjomzf
      SMTP_USE_TLS: '1'
      SMTP_FROM_EMAIL: '"updatehub - by O.S. Systems" <no-reply@ossystems.com.br>'
    stdin_open: true
    entrypoint:
    - /usr/local/bin/bundle
    tty: true
    links:
    - postgres:postgres
    - redis:redis
    - rabbitmq:rabbitmq
    command:
    - exec
    - rake
    - consumers:limits
    labels:
      io.rancher.container.pull_image: always
  api-bootstrap:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/api-server:19.2.0
    environment:
      RAILS_ENV: production
      DATABASE_HOST: postgres
      DATABASE_NAME: updatehub
      DATABASE_USER: postgres
      DATABASE_PASSWORD: postgres
      PGUSER: postgres
    entrypoint:
    - /usr/local/bin/bundle
    stdin_open: true
    tty: true
    links:
    - postgres:postgres
    - hydra:hydra
    command:
    - exec
    - rake
    - db:create
    - db:migrate
    labels:
      io.rancher.container.pull_image: always
      io.rancher.container.start_once: 'true'
  postgres:
    image: postgres:10.3-alpine
    environment:
      POSTGRES_USER: postgres
      POSTGRES_PASSWORD: postgres
      PGDATA: /var/lib/postgresql/data/10
    volume_driver: '${VOLUME_DRIVER}'
    volumes:
      - postgres:/var/lib/postgresql/data
    stdin_open: true
    tty: true
    labels:
      io.rancher.container.pull_image: always
      br.com.ossystems.rancher.backup.driver: postgres
      br.com.ossystems.rancher.backup.schedule: 0 0 0 * * ?
  ui-hydra-bootstrap:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/hydra-bootstrap:19.7.0
    environment:
      CLIENT_ID: '${HYDRA_ROOT_CLIENT}'
      CLIENT_SECRET: '${HYDRA_ROOT_SECRET}'
      HYDRA_ADMIN_URL: 'http://hydra:4445'
      HYDRA_CLIENT_ID: '${UI_HYDRA_CLIENT}'
      HYDRA_CLIENT_SECRET: ''
      HYDRA_CLIENT_GRANT_TYPE: implicit
      HYDRA_CLIENT_RESPONSE_TYPE: token
      HYDRA_CLIENT_REDIRECT_URI: 'https://dashboard.${UPDATEHUB_DOMAIN}/auth/callback https://dashboard.${UPDATEHUB_DOMAIN}/silent_refresh.html'
    stdin_open: true
    tty: true
    links:
    - hydra:hydra
    command:
    - hydra-import
    labels:
      io.rancher.container.pull_image: always
      io.rancher.container.start_once: 'true'
  landing-page:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/landing-page:19.12.1
    stdin_open: true
    tty: true
    environment:
      SIGN_UP_URL: 'https://auth.${UPDATEHUB_DOMAIN}/auth/signup'
      LOG_IN_URL: 'https://dashboard.${UPDATEHUB_DOMAIN}/'
      COMMUNITY_URL: 'https://github.com/UpdateHub/updatehub-ce'
      DOCS_URL: 'https://docs.${UPDATEHUB_DOMAIN}'
      BLOG_URL: 'https://blog.${UPDATEHUB_DOMAIN}'
      MAILER_DATA_ID: '${UI_MAILER_DATA_ID}'
      MAILER_DATA_CODE: '${UI_MAILER_DATA_CODE}'
      GITHUB_URL: 'https://github.com/UpdateHub'
      GITTER_URL: 'https://gitter.im/UpdateHub/community'
      LINKEDIN_URL: 'https://www.linkedin.com/company/updatehub'
    labels:
      io.rancher.container.pull_image: always
  docs:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/docs:19.3.0
    environment:
      SITE: 'https://${UPDATEHUB_DOMAIN}'
    stdin_open: true
    tty: true
    labels:
      io.rancher.container.pull_image: always
  s3-log-collector:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/s3-log-collector:19.2.0
    environment:
      AMQP_URL: 'amqp://guest:guest@rabbitmq:5672'
      QUEUE_URL: '${S3_SERVER_ACCESS_QUEUE_URL}'
      AWS_ACCESS_KEY: '${S3_ACCESS_KEY}'
      AWS_SECRET_KEY: '${S3_SECRET_KEY}'
      AWS_REGION: '${S3_REGION}'
    links:
    - rabbitmq:rabbitmq
    labels:
      io.rancher.container.pull_image: always
  transfer-log-aggregator:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/transfer-log-aggregator:19.3.0
    environment:
      AMQP_URL: 'amqp://guest:guest@rabbitmq:5672'
      DATABASE_HOST: postgres
      DATABASE_USER: postgres
      DATABASE_PASSWORD: postgres
      DATABASE_NAME: transfer-log-aggregator
      ENV: production
    links:
    - rabbitmq:rabbitmq
    - postgres:postgres
    labels:
      io.rancher.container.pull_image: always
      io.rancher.sidekicks: transfer-log-aggregator-bootstrap
  transfer-log-aggregator-bootstrap:
    image: postgres:10.3-alpine
    environment:
      DATABASE_HOST: postgres
      DATABASE_USER: postgres
      DATABASE_NAME: transfer-log-aggregator
      PGPASSWORD: postgres
    command:
    - /bin/sh
    - -c
    - 'createdb -h $${DATABASE_HOST} -U postgres $${DATABASE_NAME}'
    links:
    - postgres:postgres
    labels:
      io.rancher.container.pull_image: always
      io.rancher.container.start_once: 'true'
  usage-monitor:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/usage-monitor:19.2.0
    environment:
      AMQP_URL: 'amqp://guest:guest@rabbitmq:5672'
      PGHOST: postgres
      PGUSER: postgres
      PGPASSWORD: postgres
      PGDATABASE: usage-monitor
      PGSSLMODE: disable
      ENV: production
      INFLUX_HOST: http://influxdb:8086
      AUTH_SERVER: http://namespace-auth:8081
    command:
    - /usr/local/updatehub/usage-monitor
    links:
    - rabbitmq:rabbitmq
    - postgres:postgres
    - influxdb:influxdb
    - namespace-auth:namespace-auth
    labels:
      io.rancher.container.pull_image: always
      io.rancher.sidekicks: usage-monitor-bootstrap
  usage-monitor-bootstrap:
    image: postgres:10.3-alpine
    environment:
      DATABASE_HOST: postgres
      DATABASE_USER: postgres
      DATABASE_NAME: usage-monitor
      PGPASSWORD: postgres
    command:
    - /bin/sh
    - -c
    - 'createdb -h $${DATABASE_HOST} -U postgres $${DATABASE_NAME}'
    links:
    - postgres:postgres
    labels:
      io.rancher.container.pull_image: always
      io.rancher.container.start_once: 'true'
  usage-monitor-reporter:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/usage-monitor:19.2.0
    environment:
      AMQP_URL: 'amqp://guest:guest@rabbitmq:5672'
      PGHOST: postgres
      PGUSER: postgres
      PGPASSWORD: postgres
      PGDATABASE: usage-monitor
      PGSSLMODE: disable
      ENV: production
      INFLUX_HOST: http://influxdb:8086
      AUTH_SERVER: http://namespace-auth:8081
    command:
    - /usr/local/updatehub/reporter
    links:
    - rabbitmq:rabbitmq
    - postgres:postgres
    - influxdb:influxdb
    - namespace-auth:namespace-auth
    labels:
      io.rancher.container.pull_image: always
  billing-consumer:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/billing-service:19.12.0
    environment:
      ENV: production
      AMQP_URL: 'amqp://guest:guest@rabbitmq:5672'
      PGHOST: postgres
      PGUSER: postgres
      PGPASSWORD: postgres
      PGDATABASE: billing-service
      PGSSLMODE: disable
      INFLUX_HOST: http://influxdb:8086
      AUTH_SERVER: http://namespace-auth:8081
      MONGO_HOST: mongo
      SLACK_TOKEN: '${SLACK_TOKEN}'
      SLACK_USER: '${SLACK_USER}'
      SLACK_CHANNEL: '${SLACK_CHANNEL}'
      FS_API_USERNAME: '${FS_API_USERNAME}'
      FS_API_PASSWORD: '${FS_API_PASSWORD}'
      FS_WEBHOOK_HMAC: '${FS_WEBHOOK_HMAC}'
      SENDER: '${BILLING_SENDER}'
      TEMPLATE_PAYMENTCONFIRMID: '${BILLING_TEMPLATE_PAYMENTCONFIRMID}'
      TEMPLATE_NAMESPACEBLOCKEDID: '${BILLING_TEMPLATE_NAMESPACEBLOCKEDID}'
      SENDGRID_API_KEY: '${BILLING_SENDGRID_API_KEY}'
    command:
    - /usr/local/updatehub/consumer
    links:
    - rabbitmq:rabbitmq
    - postgres:postgres
    - mongo:mongo
    - influxdb:influxdb
    - namespace-auth:namespace-auth
    labels:
      io.rancher.container.pull_image: always
      io.rancher.sidekicks: billing-rabbitmq-bootstrap
  billing-worker:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/billing-service:19.12.0
    environment:
      ENV: production
      AMQP_URL: 'amqp://guest:guest@rabbitmq:5672'
      PGHOST: postgres
      PGUSER: postgres
      PGPASSWORD: postgres
      PGDATABASE: billing-service
      PGSSLMODE: disable
      INFLUX_HOST: http://influxdb:8086
      AUTH_SERVER: http://namespace-auth:8081
      MONGO_HOST: mongo
      SLACK_TOKEN: '${SLACK_TOKEN}'
      SLACK_USER: '${SLACK_USER}'
      SLACK_CHANNEL: '${SLACK_CHANNEL}'
      FS_API_USERNAME: '${FS_API_USERNAME}'
      FS_API_PASSWORD: '${FS_API_PASSWORD}'
      FS_WEBHOOK_HMAC: '${FS_WEBHOOK_HMAC}'
      SENDER: '${BILLING_SENDER}'
      TEMPLATE_PAYMENTCONFIRMID: '${BILLING_TEMPLATE_PAYMENTCONFIRMID}'
      TEMPLATE_NAMESPACEBLOCKEDID: '${BILLING_TEMPLATE_NAMESPACEBLOCKEDID}'
      SENDGRID_API_KEY: '${BILLING_SENDGRID_API_KEY}'
    command:
    - /usr/local/updatehub/worker
    links:
    - rabbitmq:rabbitmq
    - postgres:postgres
    - mongo:mongo
    - influxdb:influxdb
    - namespace-auth:namespace-auth
    labels:
      io.rancher.container.pull_image: always
  billing-scheduler:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/billing-service:19.12.0
    environment:
      ENV: production
      AMQP_URL: 'amqp://guest:guest@rabbitmq:5672'
      PGHOST: postgres
      PGUSER: postgres
      PGPASSWORD: postgres
      PGDATABASE: billing-service
      PGSSLMODE: disable
      INFLUX_HOST: http://influxdb:8086
      AUTH_SERVER: http://namespace-auth:8081
      MONGO_HOST: mongo
      SLACK_TOKEN: '${SLACK_TOKEN}'
      SLACK_USER: '${SLACK_USER}'
      SLACK_CHANNEL: '${SLACK_CHANNEL}'
      FS_API_USERNAME: '${FS_API_USERNAME}'
      FS_API_PASSWORD: '${FS_API_PASSWORD}'
      FS_WEBHOOK_HMAC: '${FS_WEBHOOK_HMAC}'
      SENDER: '${BILLING_SENDER}'
      TEMPLATE_PAYMENTCONFIRMID: '${BILLING_TEMPLATE_PAYMENTCONFIRMID}'
      TEMPLATE_NAMESPACEBLOCKEDID: '${BILLING_TEMPLATE_NAMESPACEBLOCKEDID}'
      SENDGRID_API_KEY: '${BILLING_SENDGRID_API_KEY}'
    command:
    - /usr/local/updatehub/scheduler
    links:
    - rabbitmq:rabbitmq
    - postgres:postgres
    - mongo:mongo
    - influxdb:influxdb
    - namespace-auth:namespace-auth
    labels:
      io.rancher.container.pull_image: always
  billing-reporter:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/billing-service:19.12.0
    environment:
      ENV: production
      AMQP_URL: 'amqp://guest:guest@rabbitmq:5672'
      PGHOST: postgres
      PGUSER: postgres
      PGPASSWORD: postgres
      PGDATABASE: billing-service
      PGSSLMODE: disable
      INFLUX_HOST: http://influxdb:8086
      AUTH_SERVER: http://namespace-auth:8081
      MONGO_HOST: mongo
      SLACK_TOKEN: '${SLACK_TOKEN}'
      SLACK_USER: '${SLACK_USER}'
      SLACK_CHANNEL: '${SLACK_CHANNEL}'
      FS_API_USERNAME: '${FS_API_USERNAME}'
      FS_API_PASSWORD: '${FS_API_PASSWORD}'
      FS_WEBHOOK_HMAC: '${FS_WEBHOOK_HMAC}'
      SENDER: '${BILLING_SENDER}'
      TEMPLATE_PAYMENTCONFIRMID: '${BILLING_TEMPLATE_PAYMENTCONFIRMID}'
      TEMPLATE_NAMESPACEBLOCKEDID: '${BILLING_TEMPLATE_NAMESPACEBLOCKEDID}'
      SENDGRID_API_KEY: '${BILLING_SENDGRID_API_KEY}'
    command:
    - /usr/local/updatehub/reporter
    links:
    - rabbitmq:rabbitmq
    - postgres:postgres
    - mongo:mongo
    - influxdb:influxdb
    - namespace-auth:namespace-auth
    labels:
      io.rancher.container.pull_image: always
  billing-rabbitmq-bootstrap:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/billing-rabbitmq-bootstrap:be2ecd5
    environment:
      RABBITMQ_ERLANG_COOKIE: '${RABBITMQ_COOKIE}'
    labels:
      io.rancher.container.pull_image: always
      io.rancher.container.start_once: 'true'
  email-assets:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/email-assets:18.9.0
    labels:
      io.rancher.container.pull_image: always
  sendgrid-amqp-bridge:
    image: ossystems/sendgrid-amqp-bridge:19.2.3
    stdin_open: true
    tty: true
    labels:
      io.rancher.container.pull_image: always
    environment:
      CONFIG: '${SENDGRID_AMQP_BRIDGE_CONFIG}'
    links:
    - rabbitmq:rabbitmq
  influxdb:
    image: influxdb:1.5.2-alpine
    environment:
      INFLUXDB_DB: 'UHMetrics'
    labels:
      io.rancher.container.pull_image: always
    volume_driver: '${VOLUME_DRIVER}'
    volumes:
      - influxdb:/var/lib/influxdb
  fluentd:
    image: docker-registry.ossystems.com.br/ossystems/updatehub/docker/fluentd:19.3.0
    labels:
      io.rancher.container.pull_image: always
    links:
    - influxdb:influxdb
  grafana:
    image: grafana/grafana:5.3.4
    environment:
      GF_INSTALL_PLUGINS: snuids-trafficlights-panel
    labels:
      io.rancher.container.pull_image: always
    volume_driver: '${VOLUME_DRIVER}'
    volumes:
      - grafana:/var/lib/grafana
